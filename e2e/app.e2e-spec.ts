import { FrontendCodingChallengePage } from './app.po';

describe('frontend-coding-challenge App', () => {
  let page: FrontendCodingChallengePage;

  beforeEach(() => {
    page = new FrontendCodingChallengePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
