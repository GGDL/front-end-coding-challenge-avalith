import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {DropdownModule} from "ngx-dropdown";
import { HttpModule } from '@angular/http';
import { FormsModule }    from '@angular/forms';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard.component';

import { CampaignService } from './campaign.service';
import { FilterService } from './filter.service'

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    DropdownModule,
    HttpModule,
    FormsModule,
  ],
  providers: [CampaignService, FilterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
