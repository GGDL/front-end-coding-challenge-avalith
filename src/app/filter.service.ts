import { Injectable }    from '@angular/core';
import{Http,Headers} from '@angular/http'

import 'rxjs/add/operator/toPromise'

@Injectable()
export class FilterService {
  static instance: FilterService;

  constructor(private http:Http) {
    return FilterService.instance = FilterService.instance || this;
    }
    private getHeaders(){
        // I included these headers because otherwise FireFox
        // will request text/html
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        return headers;
      }

  getFilters(): Promise <Array<string>> {
  return this.http.get('http://localhost:8080/filters').
              .then()
             .subscribe(data => console.log(data));
          //   .catch(this.handleError);

  }
  getCampaign(){}

  private handleError(error: any): Promise<any> {
  console.error('An error occurred', error); // for demo purposes only
  return Promise.reject(error.message || error);
  }
}
