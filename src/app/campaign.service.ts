import { Injectable }    from '@angular/core';
import{Http,Headers} from '@angular/http'

import 'rxjs/add/operator/toPromise'

import { Campaign } from './campaign'

@Injectable()
export class CampaignService {
  static instance: CampaignService;

  constructor(private http:Http) {
    return CampaignService.instance = CampaignService.instance || this;
    }
    private getHeaders(){
        // I included these headers because otherwise FireFox
        // will request text/html
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        return headers;
      }

  getCampaigns(): Promise<Campaign[]> {
  return this.http.get('http://localhost:8080/campaigns',{headers:this.getHeaders()})
             .toPromise()
             .then(response => response.json() as Campaign[])
             .catch(this.handleError);

  }
  getCampaign(){}

  private handleError(error: any): Promise<any> {
  console.error('An error occurred', error); // for demo purposes only
  return Promise.reject(error.message || error);
  }
}
