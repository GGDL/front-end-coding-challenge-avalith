
import { Component,OnInit} from '@angular/core';
import { CampaignService } from './campaign.service'
import { FilterService } from './filter.service'
import { Campaign } from'./campaign';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  campaigns:Campaign[];
  filters=[];

  constructor(private campaignService:CampaignService, private filterService:FilterService) { }

  getCampaigns(): void {
  this.campaignService.getCampaigns().then(campaigns => this.campaigns = campaigns );
  }
  getFilters(): void {
  this.filterService.getFilters().then(filters => this.filters = filters);
  }

  ngOnInit(): void {
    this.getCampaigns();
    this.getFilters();
  }
}
