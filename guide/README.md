# Frontend Coding Challenge

This is an AngularJS / ReactJS + HTML / CSS coding challenge. This task is divided in 3 parts. First 2 are mandatory and 3rd part is a bonus task. Put your code on Github and submit a repository link. 

What we are looking for:

* Attention to detail. 
* Creativity.
* GIT practices.

## 1. Layout - Responsive Design

1. Implement design of `Dashboard.jpg` attached using FlexBox Layout (https://css-tricks.com/snippets/css/a-guide-to-flexbox/). Cards data is provided in `cards.json` attached.
2. The design should be responsive. You are not provided with mobile designs. Please implement responsive design according to your own intuition.
3. Design and implement Campaigns selection dropdown (List of campaigns is provided in `campaigns.json` attached).
4. Implement Status filter (List of statuses is provided in `filters.json` attached). There is no design for this component, the look and feel is up to you. Take into account its possible behavior.
5. Design and implement Search component. It needs to provide suggestions while you type into the field. This component will get results based on `carTitle` and `cardDescription`.

_DateTime Picker and Card actions do not need to be functional_

What we are looking for here:

* Please make the desktop design as close to provide JPG as possible
* HTML structure
* Structure for CSS classes
* Your own design sense

## 2. Functionality

* User should be able to filter cards shown based on filters mentioned above. 
* Implement a service in order to get data from the service, thus creating a singleton.
* Iterate over `cards.json` in order to show all the cards.

What we are looking for here:

* Component based structure.
* JavaScript coding style and structure.
* Correct state managment.

## 3. Bonus - NodeJS express server

Instead of hardcoding `campaigns.json`, `cards.json` and `filters.json` in frontend, put them at backend and write API's to fetch campaigns dropdown data and cards from backend. 

What we are looking for here:

* RESTful use of API's
* Usage of API's in AngularJS 2.0 / React JS

